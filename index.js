const express = require('express')
const mongoose = require('mongoose')
const Todo = require('./models/Todo')
const bodyParser = require('body-parser');
const cors = require('cors')
const todoRoutes = express.Router();

const PORT = 3003
const app = express()


app.use(cors());
app.use(bodyParser.json());
app.use(todoRoutes)

async function start() {
    try {
        await mongoose.connect(
            'mongodb+srv://Alexis0181:4y6aTblu@cluster-test.wvett.mongodb.net/todo-list?retryWrites=true&w=majority', {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        }
        )
        app.listen(PORT, () => {
            console.log(`server has been started on port ${PORT}`);
        })
    } catch (e) {
        console.log(e);
    }
}

start()

function success(res, payload) {
    return res.status(200).json(payload)
}

app.get("/todos", async (req, res, next) => {
    try {
        const todos = await Todo.find({})
        return success(res, todos)
    } catch (err) {
        next({ status: 400, message: "failed to get todos" })
    }
})

app.post("/todos", async (req, res, next) => {
    try {
        const todo = await Todo.create(req.body)
        return success(res, todo)
    } catch (err) {
        next({ status: 400, message: "failed to create todo" })
    }
})

app.put("/todos/:id", async (req, res, next) => {
    try {
        const todo = await Todo.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
        })
        return success(res, todo)
    } catch (err) {
        console.log(err)
        next({ status: 400, message: "failed to update todo" })
    }
})

app.patch("/todos", async (req, res, next) => {
    try {
        const { ids, check } = req.body;
        const todo = await Todo.updateMany({ _id: { $in: ids } }, { done: check });
        return success(res, todo)
    } catch (err) {
        console.log(err)
        next({ status: 400, message: "failed to update all todo" })
    }

})

app.patch("/todos/:id", async (req, res, next) => {
    try {
        const todo = await Todo.findByIdAndUpdate(req.params.id,
            { ...req.body.data })
        return success(res, todo)
    } catch (err) {
        next({ status: 400, message: "failed to edit todo" })
    }

})

app.delete("/todos", async (req, res, next) => {
    try {
        const { ids } = req.body
        await Todo.deleteMany({ _id: { $in: ids } })
        return success(res, "todo deleted!")
    } catch (err) {
        next({ status: 400, message: "failed to delete completed todo" })
    }
})

app.delete("/todos/:id", async (req, res, next) => {
    try {
        await Todo.findByIdAndRemove(req.params.id)
        return success(res, "todo deleted!")
    } catch (err) {
        next({ status: 400, message: "failed to delete todo" })
    }
})


app.use((err, req, res, next) => {
    return res.status(err.status || 400).json({
        status: err.status || 400,
        message: err.message || "there was an error processing request",
    })
})

